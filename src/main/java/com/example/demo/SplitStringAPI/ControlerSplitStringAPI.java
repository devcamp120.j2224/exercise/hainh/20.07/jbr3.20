package com.example.demo.SplitStringAPI;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    public class ControlerSplitStringAPI {
        @GetMapping("/split")
        public ArrayList<String> getSplitList() {
            String str = "từ dài nhất vũ trụ";
  
            // split string by no space
            String[] strSplit = str.split("");
      
            // Now convert string into ArrayList
            ArrayList<String> strList = new ArrayList<String>(
                Arrays.asList(strSplit));

                return strList;
    }
}
